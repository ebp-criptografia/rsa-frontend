import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PrincipalService } from './services/principal.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

    primoP = undefined;
    primoQ = undefined;

    n = undefined;
    fiN = undefined;
    clavePublicaE = undefined;
    clavePrivadaD = undefined;

    textACifrar = undefined;
    textCifrado = undefined;
    textADescifrar = undefined;
    textDescifrado = undefined;

    constructor(
        private formBuilder: FormBuilder,
        private principalService:PrincipalService
    ) {

    }

    ngOnInit(): void {

    }

    btnGenerarParPrimos(): void{
        this.principalService.generarPrimos().subscribe(response => {
            this.primoP = response.primoP
            this.primoQ = response.primoQ
        });
    }

    btnCalcularModuloMYParDeClaves(): void{
        this.principalService.generar_n_fiN_publica_privada({
            primoP : this.primoP,
            primoQ : this.primoQ
        }).subscribe(response => {
            this.n = response.n
            this.fiN = response.fiN 
            this.clavePublicaE = response.publica
            this.clavePrivadaD = response.privada
        });
    }

    btnCifrarMensaje(): void {
        this.principalService.cifrarMensaje({
            mensaje : this.textACifrar,
            publica : this.clavePublicaE,
            n: this.n
        }).subscribe(response => {
            this.textCifrado = response.mensaje
        });
    }

    btnDescifrarMensaje(): void {
        this.principalService.descifrarMensaje({
            mensaje : this.textADescifrar,
            privada : this.clavePrivadaD,
            n: this.n
        }).subscribe(response => {
            this.textDescifrado = response.mensaje
        });
    }

    btnLimpiar(): void {
        this.primoP = undefined;
        this.primoQ = undefined;
    
        this.n = undefined;
        this.fiN = undefined;
        this.clavePublicaE = undefined;
        this.clavePrivadaD = undefined;
    
        this.textACifrar = undefined;
        this.textCifrado = undefined;
        this.textADescifrar = undefined;
        this.textDescifrado = undefined;
    }
}
