import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment as env } from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})
export class PrincipalService {

    private endPointGenerarPrimos: string = env.urlServicesBackend + '/api/generar_primos';
    private endPointGenerarNFiNPublicaPrivada: string = env.urlServicesBackend + '/api/generar_n_fin_publica_privada';
    private endPointCifrarMensaje: string = env.urlServicesBackend + '/api/cifrar_mensaje';
    private endPointDescifrarMensaje: string = env.urlServicesBackend + '/api/descifrar_mensaje';


    constructor(private http: HttpClient) { }

    generarPrimos(): Observable<any> {
        return this.http.get<any>(this.endPointGenerarPrimos);
    }

    generar_n_fiN_publica_privada(parPrimos: any): Observable<any> {
        return this.http.post<any>(this.endPointGenerarNFiNPublicaPrivada, parPrimos);
    }

    cifrarMensaje(datos: any): Observable<any> {
        return this.http.post<any>(this.endPointCifrarMensaje, datos);
    }

    descifrarMensaje(datos: any): Observable<any> {
        return this.http.post<any>(this.endPointDescifrarMensaje, datos);
    }

}
